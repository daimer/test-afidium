<?php

namespace Mock;

use App\Providers\Streamable;
use App\Parser\StreamInterface as Stream;

/**
 *
 */
class StreamParser implements Stream {

  protected $open = true;

  /**
   *
   */
  public function setHandler(Streamable $_handler) {
    $this->handler = $_handler;
    return $this;
  }

  /**
   *
   */
  public function isOpen() {
    return $this->open;
  }

  /**
   *
   */
  public function isClosed() {
    return !$this->open;
  }

  /**
   *
   */
  public function stream() {
    $this->makeParser();
    return $this->streamResource();
  }

  /**
   *
   */
  protected function makeParser() {
    $this->parser = xml_parser_create();

    if ($this->handler) {
      xml_set_element_handler(
        $this->parser,
        [$this->handler, "handleOpenTag"],
        [$this->handler, "handleCloseTag"]
      );

      xml_set_character_data_handler(
        $this->parser,
        [$this->handler, "handleCharData"]
      );
    }

    return $this->parser;
  }


  /**
   *
   */
  protected function streamResource() {
    xml_parse($this->parser, $content = file_get_contents(__DIR__.'/hotel.xml'));
    $this->open = false;
    return $content;
  }
}
