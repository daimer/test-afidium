<?php

namespace App\Formatter;


/**
 *
 */
class Bonotel {

  /**
   *
   */
  protected $dataTree;

  /**
   *
   */
  protected $formatted = [
    "language"        => "EN",
    "latitude"        => null,
    "longitude"       => null,
    "rating_level"    => null,
    "swimmingpool"    => null,
    "parking"         => null,
    "fitness"         => null,
    "golf"            => null,
    "seaside"         => null,
    "spa"             => null,
    "charm"           => null,
    "ecotourism"      => null,
    "exceptional"     => null,
    "family_friendly" => null,
    "pmr"             => null,
    "preferred"       => null,
    "wedding"         => null,

    "distribution" => [
        "BONOTEL" => null,
    ],

    "introduction_text" => [
        "language"  => "EN",
        "type_code" => "Description",
        "title"     => "Description",
        "text"      => null
    ],

    "introduction_media" => [
        "weight" => [
            "value" => null,
            "unit"  => null
        ],
        "size" => [
            "width"  => null,
            "height" => null,
            "unit"   => "px"
        ],
        "url" => null
    ]
  ];

  /**
   *
   */
  public function __construct($_dataTree) {
    $this->dataTree = $_dataTree;
  }

  /**
   *
   */
  public function format() {

    $this->formatted["distribution"]["BONOTEL"] = $this->findTag(
      $this->dataTree, "hotelcode"
    )->value;

    $this->formatFromEquivalentTag($this->dataTree, "latitude");
    $this->formatFromEquivalentTag($this->dataTree, "longitude");

    $this->formatted["rating_level"] = (float) $this->findTag($this->dataTree, "starrating")->value;
    $this->formatted["swimmingpool"] = $this->hasValueInFacilitiesOrRecreation("/swimmingpools|\spools?/i");
    $this->formatted["parking"]      = $this->hasValueInFacilitiesOrRecreation("/parking/i");
    $this->formatted["fitness"]      = $this->hasValueInFacilitiesOrRecreation("/fitness/i");
    $this->formatted["golf"]         = $this->hasValueInFacilitiesOrRecreation("/golf/i");
    $this->formatted["seaside"]      = $this->hasValueInFacilitiesOrRecreation("/seaside/i");
    $this->formatted["wedding"]      = $this->hasValueInFacilitiesOrRecreation("/wedding/i");
    $this->formatted["ecotourism"]   = $this->hasValueInFacilitiesOrRecreation("/ecotourism/i");

    $this->formatted["introduction_text"]["text"] = $this->findTag(
      $this->dataTree, "description"
    )->value;

    $this->formatted["introduction_media"]["url"] = $this->findFirstImageUrl();

    return $this->formatted;
  }

  /**
   *
   */
  protected function hasValueInFacilitiesOrRecreation($_pattern) {
    return $this->hasValueIn([
      $this->findTag($this->dataTree, "recreation"),
      $this->findTag($this->dataTree, "facilities")
      ], $_pattern
    );
  }

  /**
   *
   */
  protected function findTag($_fragment, $_name) {
    foreach ($_fragment->children as $tag) {
      if ($tag->name === $_name) {
        return $tag;
      }
    }
  }

  /**
   *
   */
  protected function findTags($_fragment, $_name) {
    return array_filter($_fragment->children, function ($t) use ($name) {
      return $tag->name === $_name;
    });
  }

  /**
   *
   */
  protected function findFirstImageUrl() {
    $tag = $this->findTag($this->dataTree, "images");
    return $this->findTag($tag, "image")->value;
  }

  /**
   *
   */
  protected function formatFromEquivalentTag($_fragment, $_name, $_alias = null) {
    $_alias = $_alias ?: $_name;

    if ($tag = $this->findTag($this->dataTree, $_alias)) {
      $this->formatted[$_name] = $tag->value;
    }
  }

  /**
   *
   */
  protected function hasValueIn($_tags, $_regexp) {
    foreach($_tags as $tag) {
      if (preg_match($_regexp, $tag->value)) {
        return true;
      }
    }
    return false;
  }
}
