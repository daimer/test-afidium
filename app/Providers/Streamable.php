<?php

namespace App\Providers;

use App\Parser\StreamInterface as Stream;

/**
 *
 */
interface Streamable
{

  /**
   *
   */
  public function handleOpenTag($parser, $name, array $attribs = []);

  /**
   *
   */
  public function handleCloseTag($parser, $name, array $attribs = []);

  /**
   *
   */
  public function handleCharData($parser, $data);

  /**
   *
   */
  public function setStream(Stream $stream);


}
