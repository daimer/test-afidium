<?php

namespace App\Providers;

use App\Providers\Traits\Streamable as StreamableTraits;

/**
 *
 */
class Bonotel extends ProviderAbstract implements Streamable {
  use StreamableTraits;


  /**
   *
   */
  public function preProcessTag($_tag) {
    if (strtolower($_tag->name) === "hotels") {
      return null;
    }

    return $_tag;
  }

  /**
   *
   */
  public function postProcessTag($_tag) {
    if (strtolower($_tag->name) === "hotels") {
      return null;
    }

    $_tag->value = strip_tags($_tag->value);
    return $_tag;
  }
}
