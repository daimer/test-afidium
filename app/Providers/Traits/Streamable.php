<?php

namespace App\Providers\Traits;

use App\Parser\StreamInterface as Stream;
use App\Providers\Streamable as StreamableInterface;

trait Streamable {
  /**
   *
   */
  protected $stream;

  /**
   *
   */
  protected $current;

  /**
   *
   */
  protected $buffer = [];

  /**
   *
   */
  protected $currentBuildingTag;

  /**
   *
   */
  public function setStream(Stream $stream) {
    $this->stream = $stream;
    $this->stream->setHandler($this);
    return $this;
  }

  /**
   *
   */
  public function getNext() {
    if (count($this->buffer) > 0) {
      return $this->getCurrent();
    }

    if (!$this->stream) {
      return null;
    }

    if ($this->stream->isClosed()) {
      return null;
    }

    while(count($this->buffer) === 0) {
      if (null === $this->stream->stream()) {
        break;
      }
    }

    return $this->getCurrent();
  }

  /**
   *
   */
  public function handleOpenTag($parser, $name, array $attribs = []) {
    $tag = $this->preProcessTag(
      $this->createTag($name, $this->currentBuildingTag)
    );

    // Preprocess tag can return null
    if ($tag) {
      $this->currentBuildingTag = $tag;
    }
  }

  /**
   *
   */
  public function handleCloseTag($parser, $name, array $attribs = []) {
    // Preprocess tag can return null
    // So the buffer is emptied before the final tag
    $tag = $this->currentBuildingTag ? $this->postProcessTag($this->currentBuildingTag) : null;

    if (!$tag) {
      return;
    }

    if ($this->currentBuildingTag->parent === null) {
      $this->commitToBuffer($tag);
    } else {
      $this->currentBuildingTag = $tag->parent;
    }
  }

  /**
   *
   */
  public function handleCharData($parser, $data) {
    if (!$this->currentBuildingTag) {
      return;
    }

    $this->currentBuildingTag->value = $this->currentBuildingTag->value . $data;
  }

  /**
   *
   */
  protected function preProcessTag($tag) {
    return $tag;
  }

  /**
   *
   */
  protected function postProcessTag($tag) {
    return $tag;
  }

  /**
   *
   */
  protected function commitToBuffer($_tag) {
    $this->buffer[] = $_tag;
    $this->clearCurrentTag();
  }

  /**
   *
   */
  protected function getCurrent() {
    return array_shift($this->buffer);
  }

  /**
   *
   */
  protected function createTag($name, $parent, $value = "") {
    $tag = new \StdClass;
    $tag->name     = strtolower($name);
    $tag->value    = $value;
    $tag->parent   = $parent;
    $tag->children = [];

    if ($parent) {
      $parent->children[] = $tag;
    }

    return $tag;
  }

  /**
   *
   */
  protected function clearCurrentTag() {
    return $this->currentBuildingTag = null;
  }
}
