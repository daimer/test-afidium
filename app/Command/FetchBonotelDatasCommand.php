<?php
namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;

use App\Parser\Stream;
use App\Formatter\Bonotel as Formatter;
use App\Storage\File as Storage;
use App\Providers\Bonotel as BonotelProvider;

class FetchBonotelDatasCommand extends Command {

    /**
     *
     */
    protected $input;

    /**
     *
     */
    protected $output;

    /**
     *
     */
    protected $provider;

    /**
     *
     */
    protected $appConfig;

    /**
     *
     */
    protected $storage;

    /**
     *
     */
    public function setAppConfig($_config) {
      $this->appConfig = $_config;
    }

    /**
     *
     */
    protected function configure() {
        $this
          ->setName('app:fetch-bonotel-datas')
          ->setDescription('Fetch data from provider bonotel.')
          ->addOption('limit', null, InputOption::VALUE_REQUIRED, 'max entry to process (for testing mostly)')
          ->addOption(
            'exec', null, InputOption::VALUE_NONE, 'Execute the command for real'
          )->setHelp(
            'Import Data from bonotel, use --exec option to run the console'
          );
    }

    /**
     *
     */
    protected function execute(InputInterface $_input, OutputInterface $_output) {
        $this->input  = $_input;
        $this->output = $_output;

        $this->setUpCommand();
        $this->runCommand();
    }

    /**
     *
     */
    protected function setUpCommand() {
        $this->storage = new Storage($this->appConfig["storage"]);
        $this->provider = new BonotelProvider();
        $this->provider->setStream(new Stream(
          $this->appConfig["provider"]["bonotel"]
        ));
    }

    /**
     *
     */
    protected function runCommand() {
        $this->output->writeln("running...");
        $count = 0;

        while ($hotel = $this->provider->getNext()) {
          $count++;
          $formatter = new Formatter($hotel);
          $formatted = $formatter->format();
          $this->output->writeln("processing:".$formatted["distribution"]["BONOTEL"]);

          if ($this->input->getOption("exec")) {
            $this->storage->save($formatted);
          }

          if ($this->input->getOption("limit")
          &&  $count >= $this->input->getOption("limit")) {
            break;
          }
        }
    }
}
