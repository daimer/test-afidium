<?php

namespace App\Parser;

use App\Parser\Stream;

/**
 *
 */
interface StreamInterface
{

  /**
   *
   */
  public function stream();

  public function isOpen();

  public function isClosed();

}
