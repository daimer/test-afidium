<?php

namespace App\Parser;

use App\Providers\Streamable;

/**
 *
 */
class Stream implements StreamInterface {

  /**
   *
   */
  protected $uri;

  /**
   *
   */
  protected $ready;

  /**
   *
   */
  protected $closed;

  /**
   *
   */
  protected $parser;

  /**
   *
   */
  protected $handler;

  /**
   *
   */
  protected $resource;


  /**
   *
   */
  public function __construct($_uri) {
    $this->uri = filter_var($_uri, FILTER_VALIDATE_URL)
      or $this->throwException("Uri is invalid");
  }

  /**
   *
   */
  public function open() {
    return $this->openResource();
  }

  /**
   *
   */
  public function close() {
    return $this->closeResource();
  }

  /**
   *
   */
  public function setHandler(Streamable $_handler) {
    $this->handler = $_handler;
    return $this;
  }

  /**
   *
   */
  public function isReady() {
    return (bool) $this->ready;
  }

  /**
   *
   */
  public function isOpen() {
    return ((bool) $this->resource) && !$this->isClosed();
  }

  /**
   *
   */
  public function isClosed() {
    return (bool) $this->closed;
  }

  /**
   *
   */
  public function stream() {
    $this->init();
    return $this->streamResource();
  }

  /**
   *
   */
  protected function init() {
    if ($this->ready) {
      return;
    }

    $this->openResource()
    or $this->throwException("Unable to open distant resource {$this->uri}");

    $this->makeParser()
    or $this->throwException("Unable to make xml parser {$this->uri}");

    $this->ready = true;


  }

  /**
   *
   */
  protected function throwException($_msg) {
    throw new \Exception($_msg);
  }

  /**
   *
   */
  protected function makeParser() {
    $this->parser = xml_parser_create();

    if ($this->handler) {
      xml_set_element_handler(
        $this->parser,
        [$this->handler, "handleOpenTag"],
        [$this->handler, "handleCloseTag"]
      );

      xml_set_character_data_handler(
        $this->parser,
        [$this->handler, "handleCharData"]
      );
    }

    return $this->parser;
  }

  /**
   *
   */
  protected function openResource() {
    return $this->resource = @fopen($this->uri, "r");
  }

  /**
   *
   */
  protected function streamResource() {

    if ($this->closed) {
      return null;
    }

    if (feof($this->resource)) {
      xml_parse($this->parser, "", true);
      $this->closeResource();
      return null;
    }

    $content = fread($this->resource, 8192);
    xml_parse($this->parser, $content);
    return $content;
  }

  /**
   *
   */
  protected function closeResource() {
    fclose($this->resource);
    $this->resource = null;
    $this->closed = true;
  }

}
