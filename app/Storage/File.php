<?php

namespace App\Storage;

use App\Providers\Traits\Streamable as StreamableTraits;

/**
 *
 */
class File {

  /**
   *
   */
  protected $location;

  /**
   *
   */
  public function __construct($_location) {
    $this->location = $_location;
  }

  /**
   *
   */
  public function save($_hotel) {

    if (!isset($_hotel["distribution"]["BONOTEL"])) {
      throw new \Exception("Invalid format given", 1);
    }

    $hotelCode = $_hotel["distribution"]["BONOTEL"];
    $name = sprintf("HS_BNO_H_%d.json", $hotelCode);
    return file_put_contents($this->location.'/'.$name, json_encode($_hotel));
  }
}
