#!/usr/bin/env php
<?php

ini_set('max_execution_time', 300);
ini_set("memory_limit","256M");
require __DIR__.'/../vendor/autoload.php';

use Symfony\Component\Console\Application;

$config = include(realpath(__DIR__."/../config/app.php"));
$cmd = new \App\Command\FetchBonotelDatasCommand();
$cmd->setAppConfig($config);

$application = new Application();
$application->add($cmd);
$application->run();
