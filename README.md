# test-afidium

## Requirement 

PHP 7 ou supérieur

Accès à php en ligne de commande 

Composer 

## Installation 

git clone https://daimer@bitbucket.org/daimer/test-afidium.git

cd test-afidium 

composer update

composer dumpautoload -o

## Execution 

cd test-afidium

Pour obtenir de l'aide sur la commande

php app/console.php app:fetch-bonotel-datas --help

Pour executer la commande:

php app/console.php app:fetch-bonotel-datas --exec


## Tests unitaires

Pour executer les tests unitaires:

./vendor/bin/phpunit tests/Providers/BonotelTest.php 

./vendor/bin/phpunit tests/Parser/StreamTest.php 

./vendor/bin/phpunit tests/Formatter/BonotelTest.php 

## Architecture

Le problème consiste à récupérer un flux xml tiers pour le transformer dans un format métier spécifique.
Pour résoudre la problématique, nous avons repéré trois grandes responsabilités:

1/ Récupérer et analyser le flux xml

2/ Construire un arbre de données à partir du flux

3/ Post processer l'arbre obtenu afin de le convertir dans le format cible

Ces trois responsabilités correspondent aux trois classes 

1/Parser (récupération et analyse), 

2/Provider (construction de l'arbre), 

3/Formatter (conversion de l'arbre dans la structure métier cible)

L'architecture a éte concue pour pouvoir être étendus facilement à d'autres flux de donnés tiers.

## Choix techniques
Nous avons choisi d'utiliser les fonctions xml_parse plutôt que simple_xml parce que le flux d'entrée est particulièrement volumineux
Ainsi l'arbre n'a pas besoin d'être entièrement construit en mémoire, ce qui pourrait impacter fortement le serveur.
Dans cette version de test, nous n'avons pas prévu de monitoring de la mémoire, mais il serait intéressant de le faire pour s'assurer que la consommation reste faible.

## Développements manquants
Les points suivants n'ont pas été traités par manque de temps:

1/ Localisation (langue)

2/ Taille et poids de l'image

3/ Processing intelligent du langage naturel

### Localisation
Pour la localisation deux stratégies (non exclusives) sont possibles:

1/ Fournir une interface pour la traduction par un humain

2/ Utiliser une API externe type google translate pour traduire les textes

### Image
Pour récupérer la taille de l'image, une stratégie simple consisterait a télécharger l'image (temporairement)
et à utiliser les librairies fournies par imagemagick pour récupérer les informations souhaitées

### Processing intelligent du langage naturel

Il s'agit selon moi de la partie la plus intéressante du problème - que nous avons survolé par manque de temps. 
Il faut en effet déduire de textes écrits en langue naturelle des propriétés sur l'hotel - comme par exemple s'il est family friendly.
Dans le code fourni dans ce test, nous nous sommes contenté de tester des expressions régulières très basiques sur les différents textes de descriptions du flux xml
Une manière de procéder serait de développer des post processeurs spécialisés pour chaque propriétés à tester, en utilisant des regexp ou des algorithmes plus complexes d'IA
et de les faire tourner en parallèle avec un système de queue de Jobs. Il faudrait pour cela savoir ce que les librairie php propose en terme de processing de langue naturelle
ou se tourner vers des librairies ou API externes. 

Il est également possible de faire réaliser ce travail par un humain en fournissant une interface adéquate.

## Remarque générale

Nous avons tenté de proposer une architecture qui soit facilement extensible aussi bien du point de vue des flux xml tiers
que des post processing des données générées par ces flux. Pour aller plus loin, nous aurions sans doute opté pour des post processing en plusieurs passes
par des post processeurs spécialisés fonctionnant en parallèle.

Bonne lecture de code.

André Meira

