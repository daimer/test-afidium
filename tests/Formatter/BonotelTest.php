<?php

namespace Test\Formatter;

use Mock\StreamParser;
use App\Formatter\Bonotel;
use App\Providers\Bonotel as Provider;
use PHPUnit\Framework\TestCase;

class BonotelTest extends TestCase {

  /**
   *
   */
  public function testFormat() {
    $stream = new StreamParser();
    $provider = new Provider();
    $provider->setStream($stream);

    $hotel = $provider->getNext();
    $this->assertTrue(is_array($hotel->children));
    $this->assertNotEmpty($hotel->children);

    $formatter = new Bonotel($hotel);
    $formatted = $formatter->format();
    $this->assertNotEmpty($formatted);
    $this->assertTrue(is_array($formatted));

    $this->assertTrue(isset($formatted["introduction_text"]["text"]));
    $this->assertNotEmpty($formatted["introduction_text"]["text"]);

    $this->assertTrue(isset($formatted["distribution"]["BONOTEL"]));
    $this->assertNotEmpty($formatted["distribution"]["BONOTEL"]);

    $this->assertTrue(isset($formatted["swimmingpool"]));
    $this->assertTrue($formatted["swimmingpool"]);
  }
}
