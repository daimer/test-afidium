<?php

namespace Test\Parser;

use PHPUnit\Framework\TestCase;
use App\Parser\Stream;

class StreamTest extends TestCase {

  /**
   *
   */
  public function testOpenReadAndCloseStream() {
    $stream = new Stream('http://api.bonotel.com/index.cfm/user/voyagrs_xml/action/hotel');
    $stream->open();
    $this->assertTrue($stream->isOpen());
    $this->assertNotEmpty($stream->stream());

    $stream->close();
    $this->assertTrue($stream->isClosed());
  }
}
