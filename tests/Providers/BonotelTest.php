<?php

namespace Test\Providers;

use Mock\StreamParser;
use PHPUnit\Framework\TestCase;
use App\Providers\Bonotel;

class BonotelTest extends TestCase {

  /**
   *
   */
  public function testParse() {
    $stream = new StreamParser();
    $provider = new Bonotel();
    $provider->setStream($stream);

    $hotelOne = $provider->getNext();
    $this->assertTrue(is_array($hotelOne->children));
    $this->assertNotEmpty(is_array($hotelOne->children));

    $hotelTwo = $provider->getNext();
    $this->assertTrue(is_array($hotelTwo->children));
    $this->assertNotEmpty(is_array($hotelTwo->children));

    $keyOne = array_filter($hotelOne->children, function ($t) {
      return $t->name === "hotelcode";
    })[0];

    $keyTwo = array_filter($hotelTwo->children, function ($t) {
      return $t->name === "hotelcode";
    })[0];

    $this->assertNotEquals($keyOne->value, $keyTwo->value);
  }
}
